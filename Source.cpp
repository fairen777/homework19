#include<iostream>

using namespace std;

class Animal
{
private:
	
public:
	Animal() {}

	void virtual voice()
	{
		cout << "\n voice" << endl;
	}
};

class Cat :public Animal
{
public:
	void voice() override
	{
		cout << "\n Meow" << endl;
	}
};

class Dog :public Animal
{
public:
	void voice() override
	{
		cout << "\n Woof" << endl;
	}
};

class Cow :public Animal
{
public:
	void voice() override
	{
		cout << "\n Moo" << endl;
	}
};

int main()
{
	Animal* ani2 = new Animal; //test 
	ani2->voice();

	Cat cat1;
	Dog dog1;
	Cow cow1;

	const int N = 3;
	Animal* array[N] ={&cat1, &dog1, &cow1};

	for (int i = 0; i < N; i++)
	{
		array[i]->voice();
	}


	return 0;
}